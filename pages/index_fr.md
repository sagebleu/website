# Apprendre l'algèbre vous fera grand bien !

Ceci est un manuel de maths. Notre objectif est d'adopter une approche
informelle bien que logique et rigoureuse.

Le titre (et le contenu) est inspiré du livre [Learn You a Haskell for
Great Good!][lyah] (il existe une [traduction française de Valentin
Robert][lyah_fr]) de Miran Lipovača. LYAH date un peu hélas, je ne le
recommenderais pas. Je proposerais plutôt [bitemyapp's guide][bmag] (en
anglais).

[lyah]: http://learnyouahaskell.com/
[lyah_fr]: http://lyah.haskell.fr/
[bmag]: https://github.com/bitemyapp/learnhaskell
[webchat]: https://webchat.freenode.net/?channels=%23lysa

J'ai pu constater que la plupart des ouvrages de maths (et des autres sciences)
privilégiaient un style académique au détriment de la compréhension du lecteur
et, surtout, de son plaisir de lire. Nous adoptons l'approche opposée.
Nous voulons écrire un livre plaisant à lire, facile à comprendre, et cela tout
en abordant les concepts les plus avancés des mathématiques.

Si vous aimez les discussions sur IRC, venez donc nous voir sur `#lysa`,
sur Freenode (en anglais). Si vous ne savez pas ce qu'est IRC, ou bien que vous
n'avez pas de client IRC, vous pouvez toujours vous connecter <i>via</i> le
[webchat de Freenode][webchat]. Vous pouvez m'envoyer un mail : (pharpend) à
l'adresse `peter@harpending.org`. Sur le canal IRC, je suis `pharpend`.

Si vous avez des questions concernant LYSA (ou les maths), n'hésitez pas à
les poser sur le canal, ou bien sur le issue tracker.

Nous aurons probablement un blog d'ici peu.

Le mainteneur de ce site est Nick Chambers, joignable à l'adresse
`DTSCode@gmail.com`.

# Licence

LYSA est sous licence [GNU Free Documentation License][ccsa]. Cela signifie,
entre autres :

* Premièrement : vous êtes libre de lire ce livre, de le redistribuer, de le
modifier, de le vendre.
* Vous devez respecter la paternité, vous ne pouvez pas prétendre que vous avez
écrit le livre par vous-même.
* Secondement : vous êtes invité à apporter des modifications, mais vous devez
 publier vos modifications sous la même licence. Cela nous assure de pouvoir
 intégrer vos modifications dans la version principale.

Si vous voulez plus de détails vous pouvez consulter la [licence][ccsa]

[ccsa]: http://www.gnu.org/licenses/fdl.html

Le contenu de ce site est sous licence
[Creative CommonsAttribution-NoDerivatives 4.0 InternationalLicense](http://creativecommons.org/licenses/by-nd/4.0/legalcode).
